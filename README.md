# share

Social share module  
Example: http://share.module.frontend.production.adwatch.ru/

## Install

```sh
$ npm install --save @adwatch/share
```

## Usage

```js
import Share from '@adwatch/share';		// for es6

var Share = require('@adwatch/share/build');	// for es5


let share = new Share(options);
```

## Social types

`fb` - facebook.com  
`vk` - vk.com  
`ok` - odnoklassniki.ru  
`tw` - twitter.com  
`gp` - google +  
`mr` - mailRu  


## Get started

#### With html data attributes
```html
<div class="share" data-share="" data-share-img="/img/share.jpg" data-share-title="Test page" data-share-description="Test page description" data-share-url="https://test.page">
	<div class="share__item">
		<div class="share__item-ico" data-share-type="vk"><span>VK</span>
			<div class="share__item-count" data-share-count="vk"></div>
		</div>
	</div>
	<div class="share__item">
		<div class="share__item-ico" data-share-type="ok"><span>OK</span>
			<div class="share__item-count" data-share-count="ok"></div>
		</div>
	</div>
	<div class="share__item">
		<div class="share__item-ico" data-share-type="fb"><span>FB</span>
			<div class="share__item-count" data-share-count="fb"></div>
		</div>
	</div>
	<div class="share__item">
		<div class="share__item-ico" data-share-type="tw"><span>TW</span></div>
	</div>
	<div class="share__item">
		<div class="share__item-ico" data-share-type="gp"><span>G+</span>
			<div class="share__item-count" data-share-count="gp"></div>
		</div>
	</div>
	<div class="share__item">
		<div class="share__item-ico" data-share-type="mr"><span>MR</span></div>
	</div>
</div>

```
```js
let share = new Share();
```

#### Without html data attributes
```html

<div class="share">
	<div class="share__item share__item_vk">
		<div class="share__item-ico"><span>VK</span>
			<div class="share__item-count"></div>
		</div>
	</div>
	<div class="share__item share__item_ok">
		<div class="share__item-ico"><span>OK</span>
			<div class="share__item-count"></div>
		</div>
	</div>
	<div class="share__item share__item_fb">
		<div class="share__item-ico"><span>FB</span>
			<div class="share__item-count"></div>
		</div>
	</div>
	<div class="share__item share__item_tw">
		<div class="share__item-ico"><span>TW</span></div>
	</div>
	<div class="share__item share__item_gp">
		<div class="share__item-ico"><span>G+</span>
			<div class="share__item-count"></div>
		</div>
	</div>
	<div class="share__item share__item_mr">
		<div class="share__item-ico"><span>MR</span></div>
	</div>
</div>
```
```js
// jQuery

let share = new Share();

$(function() {

	let data = {
		type: 'tw',
		description: 'Test page description',
		img: 'http://test.page/img.jpg',
		title: 'Test page title',
		url: 'http://test.page'
	};

	// load count 
	share.count('vk', function(val) {
		  $('.share__item_vk').find('.share__item-count').text(val);
	})

	// cusomize options share
	$('.share__item_vk').click(function() {
		share.share(data);
	});
	
	// default options share
	$('.share__item_fb').click(function() {
		share.share('fb');
	});
	
	//... etc.
	
});

```


## API

#### Options	


| Name                       |   Type   |  Description |
| :------------------------- | :--------- | :------ |
| `fbAppId                 ` | `string  ` | facebook app id for sharing custom data (img, title, description). Default - `null` |
| `dataPostfix             ` | `string  ` | data attribute postfix (example: data-share, data-share-img, ets.). Default - `share` |
| `loadCount               ` | `boolean ` | sharing counter. Default - `false` (loaded only fb, vk & ok) |
| `callbackPopupShareOpen  ` | `function` | popup open callback |
| `callbackPopupShareClosed` | `function` | popup closed callback |
| `callbackCountLoaded     ` | `function` | count loaded callback |


All option items is optional


#### Data attributes

| Name                       |   Default   |  Description |
|----------------------------|:---------|:------|
| `data-{dataPostfix}                 ` | data-share				| set on buttons wrap element |
| `data-{dataPostfix}-type            ` | data-share-type			| social type (vk, fb, tw, gp, ok), set on buttons |
| `data-{dataPostfix}-img             ` | data-share-img			| image link for sharing, set on buttons or buttons wrap element (optional)|
| `data-{dataPostfix}-title           ` | data-share-title			| title for sharing, set on buttons or buttons wrap element (optional) |
| `data-{dataPostfix}-description     ` | data-share-description	| description for sharing, set on buttons or buttons wrap element (optional) |
| `data-{dataPostfix}-url             ` | data-share-url			| optional - sharing page url, set on buttons or buttons wrap element (optional) |



### Callbacks option arguments

callbackPopupShareOpen arguments: `({ social type }, current button)`

callbackPopupShareClosed arguments: `({ social type }, current button)`

callbackCountLoaded arguments: `({ social type, count number }, current button)`

callbackError arguments: `(error code, error message)`


#### Error codes

1 - Browser blocked popups


## Methods	

#### share.share(data);
data: social type or object collection .
```js
let data = {
	description: {share description},
	img: {share img},
	title: {share title},
	type: {share type},
	url: {share url}
};
```

### share.count(type, url, callback)

Get count method.

| Name       |   Type     |  Description |
|------------|:-----------|:------|
| `type    ` | `string  ` | social type |
| `url     ` | `string  ` | page url |
| `callback `| `function` | count load callback, return count value in argument |






## License

MIT © 
